import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import common.Tokenizer;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class Main {

    private static XYChart chart;

    public static void countError(int mode) {
        TridiagonalMatrixAlgorithm algorithm;
        List<Double> h = new ArrayList<>();
        List<Double> E = new ArrayList<>();
        for (int i = 20; i < 500; i *= 2) {
            algorithm = new TridiagonalMatrixAlgorithm(i, mode);
            algorithm.solve();
            h.add(algorithm.getH());
            E.add(algorithm.countError());
        }
        chart.getStyler().setDefaultSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Line);
        chart.addSeries("Error(h) для порядка = " + mode, h, E);
    }

    public static void main(String[] args) throws Exception {
        Tokenizer input = new Tokenizer(new FileReader("input.txt"));
        int n = input.nextInt();
        chart = new XYChartBuilder().width(800).height(400).build();

        TridiagonalMatrixAlgorithm algorithm = new TridiagonalMatrixAlgorithm(n, 1);
        algorithm.solve();
        algorithm.show();
        algorithm = new TridiagonalMatrixAlgorithm(n, 2);
        algorithm.solve();
        algorithm.show();

        countError(1);
        countError(2);
        new SwingWrapper<>(chart).displayChart("Норма разности");

    }

}
