import math.TridiagonalMatrix;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import java.util.function.Function;
import java.util.stream.IntStream;

public class TridiagonalMatrixAlgorithm {

    private Function<Double, Double> A = x -> (2.0 / x);
    private Function<Double, Double> B = x -> -1.0;
    private Function<Double, Double> C = x -> 0.0;
    private Double a = 1.0;
    private Double b = 2.0;
    private Double F1 = 1.0;
    private Double D1 = 1.0;
    private Double E1 = -Math.exp(-1);
    private Double F2 = 1.5;
    private Double D2 = 1.0;
    private Double E2 = 0.0;
    private Double[] x;
    private Double[] outX;
    private Double[] y;
    private Double[] outY;
    private Double[] outU;
    private Double EPS = 1e-4;
    private double[] as;
    private double[] bs;
    private double[] cs;
    private double[] ds;
    private Function<Double, Double> u = x -> Math.exp(-x) / x;
    private int n;
    private Double h;
    private int mode;

    public TridiagonalMatrixAlgorithm(int n, int mode) {
        this.n = n;
        this.mode = mode;
        h = (b - a) / n;
        countX();
        countMatrix();
        countOutX();
        outU = Arrays.stream(outX).map(k -> u.apply(k)).toArray(Double[]::new);
    }

    private void countOutX() {
        List<Double> tmp = new ArrayList<>();
        for (double k = a; k < b + EPS; k+= EPS)
            tmp.add(k);
        outX = tmp.toArray(new Double[0]);
    }

    private void countX() {
        x = new Double[n + 1];
        x[0] = a;
        for (int i = 1; i < n + 1; i ++)
            x[i] = x[i - 1] + h;
    }

    private void countMatrix() {
        as = new double[n + 1];
        bs = new double[n + 1];
        cs = new double[n + 1];
        ds = new double[n + 1];

        if (mode == 2) {
            bs[0] = -F1 * h + D1 + D1 * (A.apply(a) - B.apply(a) * h) * (h / 2.0);
            cs[0] = A.apply(a) * D1 * h / 2.0 + D1;
            ds[0] = E1 * h + C.apply(a) * D1 * h * h / 2.0;
        }
        else {
            bs[0] = -(F1 - D1 / h);
            cs[0] = D1 / h;
            ds[0] = E1;
        }
        for (int i = 1; i < n; i++) {
            as[i] = 1 - A.apply(x[i]) * h / 2;
            bs[i] = 2 - B.apply(x[i]) * h * h;
            cs[i] = 1 + A.apply(x[i]) * h / 2;
            ds[i] = C.apply(x[i]) * h * h;
        }
        if (mode == 2) {
            as[n] = A.apply(b) * D2 * h / 2 - D2;
            bs[n] = -F2 * h - D2 + D2 * (A.apply(b) + B.apply(b) * h) * h / 2;
            ds[n] = E2 * h - C.apply(b) * D2 * h * h / 2;
        }
        else {
            as[n] = -D2 / h;
            bs[n] = -(F2 + D2 / h);
            ds[n] = E2;
        }
        if (countCondition()) {
            System.out.println("Условие диагонального преобладания выполнено");
        }
        else {
            System.out.println("Условие диагонального преобладания НЕ выполнено");
        }
    }
    public boolean countCondition() {
        for (int i = 0; i < n + 1; i++) {
            if (!(Math.abs(bs[i]) > Math.abs(as[i]) + Math.abs(cs[i]) + EPS)) {
                return false;
            }
        }
        return true;
    }

    public void solve() {

        y = TridiagonalMatrix.solve(n, as, bs, cs, ds);

        Spline spline = new Spline(x, y, EPS);
        spline.countSpline(outX);

        outY = Arrays.stream(outX).map(spline::getValue).toArray(Double[]::new);

    }

    public double countError() {
        return IntStream.range(0, outX.length).mapToDouble(x -> Math.abs(outY[x] - outU[x])).max().orElse(-1.0);
    }

    public void show() {
        XYChart chart = new XYChartBuilder().width(800).height(400).build();
        chart.getStyler().setDefaultSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Line);
        chart.addSeries("u(x)", Arrays.asList(outX), Arrays.asList(outU));
        chart.getStyler().setDefaultSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Line);
        chart.addSeries("y(x)", Arrays.asList(outX),  Arrays.asList(outY));
        new SwingWrapper<>(chart).displayChart("Порядок = " + mode);
    }

    public Double getH() {
        return h;
    }
}
