import common.Pair;
import math.Gauss;

public class Spline {

    private Pair<Double, Double>[] table;
    private Double[] X;
    private double[] a;
    private double[] b;
    private double[] c;
    private double[] d;
    private double[] S;
    private int n;
    private double EPS;

    public Spline(int n) {
        this.n = n;
        table = new Pair[n];
        X = new Double[n];
        EPS = 1e-6;
    }

    public Spline(Double[] x, Double[] y, double EPS) {
        this.n = x.length;
        table = new Pair[n];
        for (int i = 0; i < n; i++)
            table[i] = new Pair<>(x[i], y[i]);
        X = x;
        this.EPS = EPS;
        EPS = 1e-6;
    }

    public void countCoef() {
        a = new double[n];
        b = new double[n];
        d = new double[n];
        for (int i = 0; i < n; i++)
            a[i] = table[i].second();

        double[] h = new double[n];
        for (int i = 1; i < n; i++)
            h[i] = table[i].first() - table[i - 1].first();

        double[][] A = new double[n][n];
        A[0][0] = A[n - 1][n - 1] = 1.0;
        for (int i = 1; i < n - 1; i++) {
            A[i][i - 1] = h[i];
            A[i][i] = 2 * (h[i] + h[i + 1]);
            A[i][i + 1] = h[i + 1];
        }

        double[] B = new double[n];
        for (int i = 1; i < n - 1; i++)
            B[i] = 6 * (((table[i + 1].second() - table[i].second()) / h[i + 1]) -
                    ((table[i].second() - table[i - 1].second()) / h[i]));

        Gauss gauss = new Gauss(A, B);
        c = gauss.eval();

        for (int i = 1; i < n; i++) {
            d[i] = (c[i] - c[i - 1]) / h[i];
            b[i] = (h[i] / 2) * c[i] - (h[i] * h[i] / 6) * d[i] + ((table[i].second() - table[i - 1].second()) / h[i]);
        }


    }

    public double[] countSpline(Double[] outX) {
        countCoef();
        S = new double[outX.length];
        int j = 1;
        for (int i = 0; i < outX.length; i++) {
            if (outX[i] > X[j] + EPS)
                j++;
            double x = outX[i];
            x -= X[j];
            S[i] = a[j] + b[j] * x + (c[j] / 2) * x * x + (d[j] / 6) * x * x * x;
        }
        return S;
    }

    public double getValue(double x) {
        int j;
        for(j = 0; j < n - 1; j++) {
            if (x > X[j] - EPS && x < X[j + 1] - EPS)
                break;
        }
        x -= X[j];
        return a[j] + b[j] * x + (c[j] / 2) * x * x + (d[j] / 6) * x * x * x;
    }



}
